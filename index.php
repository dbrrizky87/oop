<?php 
// ambil index animal
require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';


 
$sheep = new Animal('shoun');

echo $sheep->name."<br>";
echo $sheep->leg."<br>";
echo $sheep->cold_blooded ."<br><br>";

$sungokong = new Ape('kera sakit');
echo $sungokong->get_name()."<br>";
echo $sungokong->yell()."<br>";
echo $sungokong->leg ."<br><br>";

$kodok= new Frog ('buduk');
echo $kodok->get_name()."<br>";
echo $kodok->jump()."<br>";
echo $kodok->get_legs();


?>